import {Component, Input } from '@angular/core';
import {ApiService} from './services/api.service';

@Component({
  selector: 'custom-element',
  templateUrl: './app.component.html',
  styles: ['button { font-weight: normal; display: block; margin: 0 0 1rem; color: #FFF; width: 100%; font-family: "Poppins", "Helvetica Neue", sans-serif; font-size: 12px; border: none; background: #1e88e5; padding: 10px 15px; border-radius: 4px; }',
    '.form-control label {display: block; font-size: 10px; margin: 0 0 0.5rem;}',
    '.form-control button {display: block; font-size: 14px; margin: 0.5rem 0;}',
    'button[disabled] { opacity: 0.5}',
    '.form-control  {text-align: center; display: grid; width: 100%; margin: 1rem 0; box-sizing: border-box;}',
    '.form-control input { border: 1px #CCC solid; padding: 8px 15px; background: #FFF; width: 100%; -webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;}',
    '* { box-sizing: border-box}',

    'footer img { max-width: 48px; margin: 0.5rem; text-align: center}',
    '.aiia-payment-wrapper { text-align: center; width: 100%; max-width: 240px; margin: 0.5rem auto;}',
    '.email-form { background: #f2f2f2; padding: 0.5rem; text-align: center; width: 100%;}']
})
export class AppComponent {
  title = 'Aiia Element';
  @Input() currency;
  @Input() amount;
  @Input() goods = null;
  @Input() account;

  isEmailVisible = false;
  emailAddress = null;
  isWorking = false;

  constructor(private api: ApiService) {
  }

  generatePaymentUrl(): any {
    this.isWorking = true;
    this.api.getPaymentUrl(
      this.amount,
      this.currency,
      this.emailAddress,
      this.goods,
      this.account, 'http://127.0.0.1:5001/')
      .subscribe((apiResponse: {
        data: any;
        message: string;
      }) => {
        console.log(apiResponse);
        if (confirm(`Boom! Shall we navigate to: ${apiResponse.data.authorizationUrl }?`)) {
          location.href = apiResponse.data.authorizationUrl;
        }
      }, (error) => {
        alert('Something went wrong... check your console.');
        console.error(error);
      }, () => {
        this.isWorking = false;
      });
  }

}
