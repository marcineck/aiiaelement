import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) {
  }

  getPaymentUrl(amount, currency, email, goodsName, accountId, returnUrl): Observable<any> {
    return this.httpClient.post(`${environment.api}/aiia/external/inboundPayment`, {
      amount,
      currency,
      email,
      goodsName,
      accountId,
      returnUrl
    });

  }
}
